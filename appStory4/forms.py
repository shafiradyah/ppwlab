from django import forms
from .models import Agenda

class FormAgenda(forms.Form):
    activity = forms.CharField(label="Activity")
    day = forms.CharField(label="Day")
    date = forms.DateField(widget=forms.DateInput(attrs={'type' : 'date'}))
    time = forms.TimeField(widget=forms.TimeInput(attrs={'type' : 'time'}))
    place = forms.CharField(label="Place")
    category = forms.CharField(label="Category")
