from django.urls import path
from . import views 
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [

    path('', views.homepage, name='homepage'),
    path('', views.about, name='about-me'),
    path('', views.form, name='forms'),
    path('', views.agenda, name='agenda'),
    path('', views.create_agenda, name='create_agenda'),
    path('', views.delete_new, name='delete_new'),

]

# Create your views here.
+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)