from django.shortcuts import render, redirect
from django.http import HttpResponse
from datetime import datetime, date
from .forms import FormAgenda
from .models import Agenda



def iniweb(request):
    return render(request, 'about.html', args)
    
def homepage(request):
    return render(request, 'index.html', {})
    
def about(request):
	return render (request, 'about.html', {})

def form(request):
    return render(request, 'form.html', {})

# for displaying the Schedule that have been added

def agenda(request):
    response = {}
    agendas = Agenda.objects.all()
    response = {
        "agendas" : agendas
    }
    return render(request, 'agenda.html', response)

def create_agenda(request):
    form = FormAgenda(request.POST or None)
    response = {}
    if(request.method == "POST"):
        if (form.is_valid()):
            activity = request.POST.get("activity")
            day = request.POST.get("day")
            date = request.POST.get("date")
            time = request.POST.get("time")
            place = request.POST.get("place")
            category = request.POST.get("category")

            Agenda.objects.create(
                activity=activity, 
                day=day, 
                date=date, 
                time=time, 
                place=place, 
                category=category
            )
            return redirect('agenda.html')
        else:
            return render(request, 'createagenda.html', response)
    else:
        response['form'] = form
        return render(request, 'createagenda.html', response)



def delete_new(request):
    agendas = Agenda.objects.all().delete()
    return redirect('agenda.html')