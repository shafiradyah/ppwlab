"""story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from appStory4.views import homepage as homepage
from appStory4.views import about as aboutme
from appStory4.views import form as forms
from appStory4.views import agenda as agenda
from appStory4.views import create_agenda as create_agenda
from appStory4.views import delete_new as delete_new
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include


urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', homepage, name="homepage"),
    re_path(r'^index', homepage, name="homepage"),
    re_path(r'^about', aboutme, name="about me"),
    re_path(r'^form', forms, name="forms"),
    re_path(r'^agenda', agenda, name="agenda"),
    re_path(r'^create_agenda', create_agenda, name="create_agenda"),
    re_path(r'^delete_new', delete_new, name="delete_new"),
    
]

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns += staticfiles_urlpatterns()


